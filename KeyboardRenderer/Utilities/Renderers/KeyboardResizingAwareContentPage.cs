﻿using System;
using Xamarin.Forms;

namespace KeyboardRenderer.Utilities.Renderers
{
    public class KeyboardResizingAwareContentPage : ContentPage
    {
        public bool CancelsTouchesInView = true;
        public KeyboardResizingAwareContentPage()
        {
        }
    }
}
